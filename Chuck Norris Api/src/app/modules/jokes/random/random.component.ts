import { Component, Input, OnInit } from '@angular/core';
import { IJoke } from '../../shared/model/ijoke';

@Component({
  selector: 'app-random',
  templateUrl: './random.component.html',
  styleUrls: ['./random.component.scss'],
})
export class RandomComponent implements OnInit {
  @Input() joke: IJoke = {
    icon_url: '',
    id: '',
    url: '',
    value: '',
    updated_at: '',
  };
  constructor() {}

  ngOnInit(): void {}
}
