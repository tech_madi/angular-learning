import { Component, Input, OnInit } from '@angular/core';
import { IJoke } from '../../shared/model/ijoke';

@Component({
  selector: 'app-cat-joke',
  templateUrl: './cat-joke.component.html',
  styleUrls: ['./cat-joke.component.scss'],
})
export class CatJokeComponent implements OnInit {
  @Input() categoryJoke: IJoke = {
    icon_url: '',
    id: '',
    url: '',
    value: '',
  };
  constructor() {}

  ngOnInit(): void {}
}
