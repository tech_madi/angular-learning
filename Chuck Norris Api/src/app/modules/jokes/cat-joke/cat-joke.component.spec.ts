import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CatJokeComponent } from './cat-joke.component';

describe('CatJokeComponent', () => {
  let component: CatJokeComponent;
  let fixture: ComponentFixture<CatJokeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CatJokeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CatJokeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
