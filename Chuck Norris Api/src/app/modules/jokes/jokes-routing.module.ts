import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllJokesComponent } from './all-jokes/all-jokes.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'all-jokes',
    pathMatch: 'full',
  },
  {
    path: 'all-jokes',
    component: AllJokesComponent,
  },
  {
    path: '**',
    component: AllJokesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JokesRoutingModule {}
