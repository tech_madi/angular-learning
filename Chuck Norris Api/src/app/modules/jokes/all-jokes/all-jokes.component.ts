import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ChuckService } from '../../shared/service/chuck.service';
import { IJoke } from './../../shared/model/ijoke';

@Component({
  selector: 'app-all-jokes',
  templateUrl: './all-jokes.component.html',
  styleUrls: ['./all-jokes.component.scss'],
})
export class AllJokesComponent implements OnInit, OnDestroy {
  subscription = new Subscription();
  public isMenuCollapsed = true;
  @Input() randomJoke: IJoke = {
    icon_url: '',
    id: '',
    url: '',
    value: '',
    updated_at: '',
  };
  @Input() categorys: string[] = [];
  isLoading: Boolean = true;
  showCategory: Boolean = false;
  constructor(private chuckService: ChuckService) {}

  ngOnInit(): void {
    // // this.getJokes();
    // this.getCategorys();
  }

  ngOnDestroy(): void {}

  getJokes() {
    this.subscription.add(
      this.chuckService.getJoke().subscribe((res) => {
        this.randomJoke = res;
        this.isLoading = false;
        console.log(this.randomJoke);
      })
    );
  }
  getCategorys() {
    this.subscription.add(
      this.chuckService.getJokeCategory().subscribe((res) => {
        this.categorys = res;
        this.isLoading = true;
        this.showCategory = true;
      })
    );
  }
}
