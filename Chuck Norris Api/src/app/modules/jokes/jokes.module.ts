import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllJokesComponent } from './all-jokes/all-jokes.component';
import { JokesRoutingModule } from './jokes-routing.module';
import {
  NgbCollapseModule,
  NgbDropdownModule,
} from '@ng-bootstrap/ng-bootstrap';
import { RandomComponent } from './random/random.component';
import { CategoryComponent } from './category/category.component';
import { CatJokeComponent } from './cat-joke/cat-joke.component';

@NgModule({
  declarations: [AllJokesComponent, RandomComponent, CategoryComponent, CatJokeComponent],
  imports: [
    CommonModule,
    JokesRoutingModule,
    NgbDropdownModule,
    NgbCollapseModule,
  ],
})
export class JokesModule {}
