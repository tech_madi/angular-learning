import { Component, Input, OnInit } from '@angular/core';
import { ChuckService } from '../../shared/service/chuck.service';
import { IJoke } from './../../shared/model/ijoke';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
})
export class CategoryComponent implements OnInit {
  category: string = '';
  @Input() categorys: string[] = [];
  @Input() categoryJoke: IJoke = {
    icon_url: '',
    id: '',
    url: '',
    value: '',
  };
  isLoaded: boolean = false;
  constructor(private chuckService: ChuckService) {}

  ngOnInit(): void {}
  getJokeByCategory(category: any) {
    this.chuckService.getJokeFromCategory(category).subscribe((res) => {
      this.categoryJoke = res;
      this.isLoaded = true;
      this.category = category;
    });
  }
  getCategorys() {
    this.isLoaded = false;
  }
}
