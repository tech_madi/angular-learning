import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IJoke } from './../model/ijoke';
import { environment } from './../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ChuckService {
  constructor(private http: HttpClient) {}

  getJoke(): Observable<IJoke> {
    return this.http.get<IJoke>(`${environment.server_url}random`);
  }
  getJokeCategory(): Observable<[]> {
    return this.http.get<[]>(`${environment.server_url}categories`);
  }
  getJokeFromCategory(category: string): Observable<IJoke> {
    return this.http.get<IJoke>(`${environment.server_url}/random?${category}`);
  }
  searchJoke(text: string) {
    return this.http.get(`${environment.server_url}search ?${text}`);
  }
}
