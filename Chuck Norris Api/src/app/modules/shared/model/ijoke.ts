export interface IJoke {
  icon_url: string;
  id: string;
  url: string;
  value: string;
  updated_at?: string;
}
