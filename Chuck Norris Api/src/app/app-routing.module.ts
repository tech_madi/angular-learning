import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/joke',
    pathMatch: 'full',
  },
  {
    path: 'joke',
    loadChildren: () =>
      import('./modules/jokes/jokes.module').then((m) => m.JokesModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
